package com.mutableconst.units.systems.metric;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MetricSystemTest {

    private MetricSystem metricSystem;

    @BeforeEach
    public void setup() {
        this.metricSystem = new MetricSystem();
    }

    @Test
    public void testBaseLength_meter() {
        assertEquals(MetricLength.METER, metricSystem.getBaseLength());
    }

    @Test
    public void testOtherLengths_hasAll() {
        Assertions.assertArrayEquals(MetricLength.values(), metricSystem.otherLengths().toArray());
    }
}
