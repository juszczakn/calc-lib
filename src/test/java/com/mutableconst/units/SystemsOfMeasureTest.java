package com.mutableconst.units;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SystemsOfMeasureTest {

    @Test
    public void testMetric_isNotNull() {
        assertNotNull(SystemsOfMeasure.METRIC.getSystemOfMeasure());
    }

    @Test
    public void testImperial_isNull() {
        assertNull(SystemsOfMeasure.IMPERIAL.getSystemOfMeasure());
    }
}
