package com.mutableconst.math;

import com.mutableconst.units.systems.Value;
import com.mutableconst.units.systems.metric.MetricLength;
import com.mutableconst.units.systems.usimperial.UsImperialLength;
import com.mutableconst.units.types.ILength;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorImplTest {
    private ICalculator calculatorImpl;

    @BeforeEach
    public void setup() {
        calculatorImpl = new CalculatorImpl();
    }

    @Test
    public void testCalcArea_sanity_metric() {
        Value<ILength> width = new Value<ILength>(10, MetricLength.METER);
        Value<ILength> height = new Value<ILength>(10, MetricLength.KILOMETER);

        Value<ILength> areaInM = calculatorImpl.calcArea(MetricLength.METER, width, height);
        assertEquals(new Value<ILength>(100000.00, MetricLength.METER, 2), areaInM);

        Value<ILength> areaInKm = calculatorImpl.calcArea(MetricLength.KILOMETER, width, height);
        assertEquals(new Value<ILength>(100.0, MetricLength.KILOMETER, 2), areaInKm);
    }

    @Test
    public void testCalcArea_metric_to_us_imperial() {
        Value<ILength> width = new Value<ILength>(10, MetricLength.METER);
        Value<ILength> height = new Value<ILength>(10, MetricLength.KILOMETER);

        Value<ILength> areaInYards = calculatorImpl.calcArea(UsImperialLength.YARD, width, height);
        assertEquals(new Value<ILength>(109361.33, UsImperialLength.YARD, 2), areaInYards);

        Value<ILength> areaInM = calculatorImpl.calcArea(UsImperialLength.INCH, width, height);
        assertEquals(new Value<ILength>(3937007.88, UsImperialLength.INCH, 2), areaInM);
    }
}
