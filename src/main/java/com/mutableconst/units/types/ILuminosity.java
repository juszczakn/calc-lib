package com.mutableconst.units.types;

public interface ILuminosity extends IUnit {
    ILuminosity getBase();
}
