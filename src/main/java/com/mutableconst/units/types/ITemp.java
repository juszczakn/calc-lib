package com.mutableconst.units.types;

public interface ITemp extends IUnit {
    ITemp getBase();
}
