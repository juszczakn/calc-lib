package com.mutableconst.units.types;

public interface ILength extends IUnit {
    ILength getBase();
}
