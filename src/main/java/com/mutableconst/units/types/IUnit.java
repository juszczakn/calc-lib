package com.mutableconst.units.types;

import java.math.BigDecimal;

/**
 * A given unit of measure, eg. a meter, a kilogram, a pound.
 * Amount of base is % of metric base amount.
 */
public interface IUnit {
    String getName();
    BigDecimal getAmountOfBase();
    IUnit getBase();
}
