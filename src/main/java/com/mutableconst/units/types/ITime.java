package com.mutableconst.units.types;

public interface ITime extends IUnit {
    ITime getBase();
}
