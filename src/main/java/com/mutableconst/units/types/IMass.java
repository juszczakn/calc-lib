package com.mutableconst.units.types;

public interface IMass extends IUnit {
    IMass getBase();
}
