package com.mutableconst.units.types;

/**
 * ie. Moles
 */
public interface IAmount extends IUnit {
    IAmount getBase();
}
