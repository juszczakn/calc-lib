package com.mutableconst.units.systems.metric;

import com.mutableconst.units.systems.ISystemOfMeasure;
import com.mutableconst.units.types.*;
import com.mutableconst.units.systems.util.CollectionUtil;

import java.util.Collection;

public class MetricSystem implements ISystemOfMeasure {

    public ITemp getBaseTemp() {
        return null;
    }

    public ITime getBaseTime() {
        return null;
    }

    public ILength getBaseLength() {
        return MetricLength.METER;
    }

    public IMass getBaseMass() {
        return null;
    }

    public ILuminosity getBaseLuminosity() {
        return null;
    }

    public IAmount getBaseAmount() {
        return null;
    }

    public Collection<ITemp> otherTemps() {
        return null;
    }

    public Collection<ITime> otherTimes() {
        return null;
    }

    public Collection<ILength> otherLengths() {
        return CollectionUtil.<ILength>intoList(MetricLength.values());
    }

    public Collection<IMass> otherMasses() {
        return null;
    }

    public Collection<ILuminosity> otherLuminosities() {
        return null;
    }

    public Collection<IAmount> otherAmounts() {
        return null;
    }
}
