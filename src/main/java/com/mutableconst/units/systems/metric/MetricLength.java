package com.mutableconst.units.systems.metric;

import com.mutableconst.units.types.ILength;

import java.math.BigDecimal;

public enum MetricLength implements ILength {
    METER("meter", 1)
    , MM("millimeter", 0.001)
    , CM("centimeter", 0.01)
    , KILOMETER("kilometer", 1000)
    ;

    private String name;
    private BigDecimal ofBase;

    MetricLength(String name, double ofBase) {
        this.name = name;
        this.ofBase = new BigDecimal(ofBase);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAmountOfBase() {
        return ofBase;
    }

    public ILength getBase() {
        return MetricLength.METER;
    }
}
