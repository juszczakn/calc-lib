package com.mutableconst.units.systems;

import com.mutableconst.units.types.IUnit;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Value<T extends IUnit> {
    private BigDecimal number;
    private T unit;

    public Value(BigDecimal number, T unit) {
        this.number = number;
        this.unit = unit;
    }

    public Value(int number, T unit) {
        this.number = new BigDecimal(number);
        this.unit = unit;
    }

    public Value(double number, T unit, int scale) {
        this.number = new BigDecimal(number).setScale(scale, RoundingMode.HALF_UP);
        this.unit = unit;
    }

    public BigDecimal getNumber() {
        return number;
    }

    public T getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return "Value{" +
                "number=" + number +
                ", unit=" + unit +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Value<?> value = (Value<?>) o;

        if (number != null ? !number.equals(value.number) : value.number != null) return false;
        return unit != null ? unit.equals(value.unit) : value.unit == null;
    }

    @Override
    public int hashCode() {
        int result = number != null ? number.hashCode() : 0;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }
}
