package com.mutableconst.units.systems.usimperial;

import com.mutableconst.units.systems.metric.MetricLength;
import com.mutableconst.units.types.ILength;

import java.math.BigDecimal;

/**
 * Everything is based off of meters, folks.
 */
public enum UsImperialLength implements ILength {
    FOOT("foot", 0.3048)
    , INCH("inch", (1.0 / 12) * 0.3048)
    , YARD("yard",  3 * 0.3048)
    , MILE("mile", 5280 * 0.3048)
    ;

    private String name;
    private BigDecimal ofBase;

    UsImperialLength(String name, double ofBase) {
        this.name = name;
        this.ofBase = new BigDecimal(ofBase);
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAmountOfBase() {
        return ofBase;
    }

    public ILength getBase() {
        return MetricLength.METER;
    }
}
