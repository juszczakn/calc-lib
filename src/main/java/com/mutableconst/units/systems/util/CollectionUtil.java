package com.mutableconst.units.systems.util;

import com.mutableconst.units.types.IUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollectionUtil {
    /**
     * Convert array to list.
     * @param iUnits nullable
     * @return [] if null or [iUnits]
     */
    public static <T extends IUnit> List<T> intoList(T[] iUnits) {
        if (iUnits == null) {
            return new ArrayList<T>();
        }
        return new ArrayList<T>(Arrays.asList(iUnits));
    }
}
