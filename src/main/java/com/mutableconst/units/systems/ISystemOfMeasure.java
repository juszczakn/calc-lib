package com.mutableconst.units.systems;

import com.mutableconst.units.types.*;

import java.util.Collection;

/**
 * Everything is based on % of Metric for now I think.
 */
public interface ISystemOfMeasure {
    // these are all % of metric's base units
    ITemp getBaseTemp();
    ITime getBaseTime();
    ILength getBaseLength();
    IMass getBaseMass();
    ILuminosity getBaseLuminosity();
    IAmount getBaseAmount();

    Collection<ITemp> otherTemps();
    Collection<ITime> otherTimes();
    Collection<ILength> otherLengths();
    Collection<IMass> otherMasses();
    Collection<ILuminosity> otherLuminosities();
    Collection<IAmount> otherAmounts();
}
