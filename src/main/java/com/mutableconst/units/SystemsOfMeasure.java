package com.mutableconst.units;

import com.mutableconst.units.systems.ISystemOfMeasure;
import com.mutableconst.units.systems.metric.MetricSystem;

public enum SystemsOfMeasure implements ISystemOfMeasureProvider {
    METRIC(new MetricSystem()),
    IMPERIAL(null)
    ;

    private ISystemOfMeasure systemOfMeasure;

    SystemsOfMeasure(ISystemOfMeasure systemOfMeasure) {
        this.systemOfMeasure = systemOfMeasure;
    }

    public ISystemOfMeasure getSystemOfMeasure() {
        return systemOfMeasure;
    }
}
