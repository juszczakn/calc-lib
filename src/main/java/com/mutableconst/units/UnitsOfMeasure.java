package com.mutableconst.units;

/**
 *
 * A 	ampere 	electric current
 * K 	kelvin 	temperature
 * s 	second 	time
 * m 	metre 	length
 * kg 	kilogram 	mass
 * cd 	candela 	luminous intensity
 * mol 	mole 	amount of substance
 */
public enum UnitsOfMeasure {
    TEMP,
    TIME,
    LENGTH,
    MASS,
    LUMINOSITY,
    AMOUNT
}
