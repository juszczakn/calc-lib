package com.mutableconst.units;

import com.mutableconst.units.systems.ISystemOfMeasure;

public interface ISystemOfMeasureProvider {
    ISystemOfMeasure getSystemOfMeasure();
}
