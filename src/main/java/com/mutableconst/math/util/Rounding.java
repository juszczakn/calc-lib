package com.mutableconst.math.util;

import java.math.RoundingMode;

public class Rounding {

    /**
     * let's just use ceiling? for now.
     * @return
     */
    public static RoundingMode mode() {
        return RoundingMode.CEILING;
    }
}
