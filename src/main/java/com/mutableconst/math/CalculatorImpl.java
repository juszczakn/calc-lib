package com.mutableconst.math;

import com.mutableconst.math.util.Rounding;
import com.mutableconst.units.systems.Value;
import com.mutableconst.units.types.ILength;

import java.math.BigDecimal;

public class CalculatorImpl implements ICalculator {

    public Value<ILength> calcArea(ILength result, Value<ILength> w, Value<ILength> h) {
        final BigDecimal width = w.getNumber().multiply(w.getUnit().getAmountOfBase());
        final BigDecimal height = h.getNumber().multiply(h.getUnit().getAmountOfBase());

        final BigDecimal area = width.multiply(height);
        final BigDecimal areaInResultUnits = area.divide(result.getAmountOfBase(), 2, Rounding.mode());

        return new Value<ILength>(areaInResultUnits, result);
    }
}
