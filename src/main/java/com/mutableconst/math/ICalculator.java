package com.mutableconst.math;

import com.mutableconst.units.systems.Value;
import com.mutableconst.units.types.ILength;

public interface ICalculator {
    Value<ILength> calcArea(ILength result, Value<ILength> w, Value<ILength> h);
}
